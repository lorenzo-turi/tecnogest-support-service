﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tecnogest
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        protected override bool OnBackButtonPressed() {
            if (WebViewElement.CanGoBack) {
                WebViewElement.GoBack();
                return true;
            } else {
                return false;
            }
        }
	}
}
